export default class clase {

  /**
     * Procces the data and returns the clases attribute
     * @param data the data object that contains clases
     * @returns {object} the array of objects clases
     */
    getClasesInput = (data) => {
      var clases = data.clases;
      return clases;
  }

  /**
   * Creates a new clase object
   * @param {string} idProfesor the id of the profesor. <nombre.apellido>
   * @param {string} apellidos the las name of the profesor <apellidos>
   * @param {int} numAlumnos the number of alumnos in class
   * @param {object} grupos the object array containing grupos
   * @returns {object} a new clase with its attributes
   */
  createClase = (idProfesor, apellidos, numAlumnos, grupos) => {
      var clase = {
          profesor: idProfesor,
          apellidos_profesor: apellidos,
          total_alumnos: numAlumnos //,
          // FIXME - Grupos deberia ser un array de objetos
          /*
          grupos: grupos
          */
      }
      return clase;
  }

  /**
   * Creates a new grupo object
   * @param {string} nombre the name of the group <IIAA3201>
   * @param {string} asignatura the name of the assignment <Inglés III>
   * @param {int} numAlumnos the number of students in the group
   * @returns {object} a new grupo object with its attributes
   */
  createGrupo = (nombre, asignatura, numAlumnos) => {
      var grupo = {
          nombre_grupo: nombre,
          nombre_asignatura: asignatura,
          total_alumnos_grupo: numAlumnos
      }
      return grupo;
  }
  
}