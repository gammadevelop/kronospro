export default class aula {

   /**
     * Creates a new aula object
     * @param {string} nombre the name of the classroom
     * @param {string} planta the floor of the classroom
     * @param {int} capacidad the capacity of the classroom
     * @param {string} desc the description of the classroom
     * @param {object} caracteristicas the array of characteristics
     * @returns {object} a new aula object with its attributes
     */
    createAula = (nombre, planta, capacidad, desc, caracteristicas) => {
      var aula = {
          nombre: nombre,
          planta: planta,
          capacidad: capacidad,
          descripcion: desc,
          caracteriticas: caracteristicas
      }
      return aula;
  }
}