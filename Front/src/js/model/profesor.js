export default class profesor {

  //#region Data profesores

    /**
     * Procces the data and returns the profesor attribute
     * @param {object} data the data object that contains profesores
     * @returns {object} the array of objects profesores
     */
    getProfesoresInput = (data) => {
      var profesores = data.profesores;
      return profesores;
  }

  /**
   * Create a new profesor object
   * @param {string} id the id of the profesor. <nombre.apellido>
   * @param {string} nombre the name of the profesor <nombre1>
   * @param {string} apellidos the last name of the profesor <apellidos1>
   * @param {object} disponibilidad the availability object of the profesor
   * @returns {object} a new profesor with its attributes
   */
  createProfesor = (id, nombre, apellidos, disponibilidad) => {
      var prof = {
          id: id,
          nombre: nombre,
          apellidos: apellidos,
          disponibilidad: disponibilidad
      }
      return prof;
  }

  //#endregion

}