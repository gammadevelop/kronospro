const c = require('../model/profesor');
export default class teacherFormController {
  profesorManager;

  startWorkTime = 8;
  endWorkTime = 21;
  daysWork = 5;

  constructor() {
    //var c = require('../model/profesor');
    this.profesorManager = new c.default();

    this.createDummyData();

  }

  profesores = [];

  getProfesores = () => {
    // console.log('GetProfesores');
    // console.log(this.profesores[0].disponibilidad);
    return Array.from(this.profesores);
  }

  setProfesores = (profesores) => {
    // console.log('SetProfesores');
    this.profesores = profesores
    // console.log(this.profesores[0].disponibilidad);
  }


  createDummyData = () => {
    this.addNewProfesor('jon.snow', 'Jon', 'Snow', this.getEmptyTableCells());
    this.addNewProfesor('sansa.stark', 'Sansa', 'Stark', this.getEmptyTableCells());
    this.addNewProfesor('daenerys.targaryen', 'Daenerys', 'Targaryen', this.getEmptyTableCells());
    this.addNewProfesor('eddard.stark', 'Eddard', 'Stark', this.getEmptyTableCells());
  }

  getEmptyTableCells = () => {
    var r = [
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0]
    ];
    return r;
  }

  getWorkTime = () => {
    var time = {
      startTime: this.startWorkTime,
      endTime: this.endWorkTime,
      days: this.daysWork,
      totalTime: (this.endWorkTime - this.startWorkTime)
    }
    return time;
  }

  /**
   * Adds a new profesor object to this.profesores array
   * @param {string} id the id of the profesor. <nombre.apellido>
   * @param {string} nombre the name of the profesor <nombre1>
   * @param {string} apellidos the last name of the profesor <apellidos1>
   * @param {object} disponibilidad the availability object of the profesor
   */
  addNewProfesor = (id, nombre, apellidos, disponibilidad) => {
    //var profesor = this.createProfesor(id, nombre, apellidos, disponibilidad);
    var profesor = this.profesorManager.createProfesor(id, nombre, apellidos, disponibilidad);
    this.addProfesor(profesor);
  }

  /**
   * Adds a profesor object to this.profesores array
   * @param profesor the profesor object
   */
  addProfesor = (profesor) => {
    this.profesores.push(profesor);
  }

  saveProfesor = (profesor) => {
    // console.log('S_Save');
    var profesores = this.getProfesores();
    var added = -1;
    for (let i = 0; i < profesores.length; i++) {
      const e = profesores[i];
      if (e.id === profesor.id) {
        profesores[i] = profesor;
        added = i;
        break;
      }

      console.log(profesores);
    }

    if (added === -1) {
      profesores.push(profesor);
      added = profesores.length - 1;
    }

    this.setProfesores(profesores);
    // console.log('E_Save');
    return added;
  }

  createProfesor = (id, nombre, apellidos, disponibilidad) => {
    return this.profesorManager.createProfesor(id, nombre, apellidos, disponibilidad)
  }

}