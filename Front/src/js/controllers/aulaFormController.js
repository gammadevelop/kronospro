const c = require('../model/aula');
export default class aulaFormController {
  aulaManager;

  constructor(){
    //var c = require('../model/aula');
    this.aulaManager = new c.default();
  
    this.createDummyData();

  }

  /**
   * Creates and sets Dummy data for testing
   */
  createDummyData = () => {
    this.addNewAula('-2.4', '-2', 50, 'Descripcion del aula', ['Ordenadores']);
    this.addNewAula('-2.2', '-2', 50, 'Descripcion del aula', ['Proyector']);
    this.addNewAula('-1.6', '-1', 50, 'Descripcion del aula', ['Sillas']);
    this.addNewAula('1.8', '1', 50, 'Descripcion del aula', ['Ordenadores', 'Proyector', 'Sillas']);
    this.addNewAula('3.10', '3', 50, 'Descripcion del aula', ['Ordenadores', 'AlgoMas']);
  }

  aulas = [];
  plantas = [
    '-2',
    '-1',
    '0',
    '1',
    '2',
    '3'
  ];
  caracteristicas = [
    'Ordenadores',
    'Proyector',
    'Sillas',
    'Mesas',
    'Pantalla'
  ];

  /**
   * Returns the aulas saved in this object
   * @returns {object} the aulas array object
   */
  getAulas = () => {
    return this.aulas;
  }

  /**
   * Returns the plantas available 
   * @returns {object} the array of plantas available
   */
  getplantas = () => {
    return this.plantas;
  }

  /**
   * Returns the caracteristicas available
   * @returns {object} the array of caracteristicas available
   */
  getCaracteristicas = () => {
    return this.caracteristicas;
  }

  /**
   * Returns object  with caracteristicas after 
   * adding parameters used in the view 
   * @returns {object} the array of object with caracteristicas and control value 
   */
  mapCaractForView = () => {
    var array = [];
    this.caracteristicas.forEach(e => {
      array.push({text: e, active: false});  
    });
    return array;
  }

  /**
   * Converts the object object with caracteristicas and control value 
   * used in view, to array of caracteristicas used in backend 
   * @param {object} caracteristicas the caracteristicas object from the view 
   * @returns {object} the array of caracteristicas 
   */
  mapCaractForController = (caracteristicas) => {
    var array = [];
    caracteristicas.forEach(e => {
      if(e.active)
      {
        array.push(e.text);
      }
    });
    return array;
  }

  addNewCaracteristica = (name = null) => {
    if(name)
    this.caracteristicas.push(name);
  }

  /**
   * Adds the passed aula object in to aulas object from this class
   * @param {object} aula the aula object
   * @return {int} int representing the index in the aula array where the 
   * object was placed. If it failed return -1  
   */
  saveAula = (aula) => {
    var added = -1;
    for (let i = 0; i < this.aulas.length; i++) {
      const e = this.aulas[i];
      if(e.nombre === aula.nombre)
      {
        this.aulas[i] = aula;
        added = i;
        break;
      }
    }

    if(added === -1)
    {
      this.addAula(aula);
      added = this.aulas.length - 1;
    }

    return added;  
  }

  /**
   * Adds a aula object to this.aulas array
   * @param {object} aula the aulas object
   */
  addAula = (aula) => {
    this.aulas.push(aula);
  }

  /**
   * Adds a new aula object to this.aulas array
   * @param {string} nombre the name of the classroom
   * @param {string} planta the floor of the classroom
   * @param {int} capacidad the capacity of the classroom
   * @param {string} desc the description of the classroom
   * @param {object} caracteristicas the array of characteristics
   */
  addNewAula = (nombre, planta, capacidad, desc, caracteristicas) => {
      //var aula = this.createAula(nombre, planta, capacidad, desc, caracteristicas);
      var aula = this.aulaManager.createAula(nombre, planta, capacidad, desc, caracteristicas);
      this.addAula(aula);
  }

  createAula = (nombre, planta, capacidad, desc, caracteristicas) => {
    return this.aulaManager.createAula(nombre, planta, capacidad, desc, caracteristicas)
  }
}

