/*
disponibilidad de cada PROFESOR en la semana
disponibilidad de caracteristicas y recursos de cada AULA (numero, capacidad, planta, ordenadores, etc)
login con google
*/

// TODO -Ingreso disponibilidad profesor
// TODO -Ingreso Informacion Aula
// TODO -Login con Google

import _profesor from './../model/profesor.js';
import _clase from './../model/clase.js';
import _aula from './../model/aula.js';

export default class FrontController {

    Profesor = undefined;
    Clase = undefined;
    Aula = undefined;

    constructor() {
        // var _profesor = require('./../model/profesor.js');
        this.Profesor = new _profesor.Class();
        // var _clase = require('./../model/clase.js');
        this.Clase = new _clase.Class();
        // var _aula = require('./../model/aula.js');
        this.Aula = new _aula.Class();
    }

    //#region Var declarations

    profesores = [];
    clases = [];
    aulas = [];

    //#endregion


    /**
     * Recives a json string and converts it to a object
     * @param {string} input the json string
     * @returns {object} the string converted to object. In case of error returns <(int) -1>
     */
    processJson = input => {
        var data = null;

        try {
            //input deberia de tener el formato de "Formularios_Input_SG_M.json"
            data = JSON.parse(input);
        } catch (e) {
            //console.error('Error at: frontController.processJson()\n' + e);
            return -1;
        }

        return data;
    }

    /**
     * Sends a request to the specified url.
     * @param {string} path the path to send the post request to
     * @param {object} params the paramiters to add to the url
     * @returns {int} the status code. In case of error returns <(int) -1>
     */
    post = (path, params) => {
        var xhr = new XMLHttpRequest();
        var url = path;
        try {
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.onreadystatechange = () => {
                //Should not happen
                // if (xhr.readyState === 4 && xhr.status === 200) {
                //     var json = JSON.parse(params);
                //     console.log("Recived Json: " + json);
                // }
            };
            var data = JSON.stringify(params);
            xhr.send(data);
        } catch (e) {
            //console.error('Error at: frontController.post\n' + e);
            return -1;
        }
        return xhr.status;
    }

    //#region Data clases

    /**
     * Adds a clase object to this.clases array
     * @param {object} the clase object
     */
    addClase = (clase) => {
        this.clases.push(clase);
    }

    /**
     * Adds a new clase object to this.clases array
     * @param {string} idProfesor the id of the profesor. <nombre.apellido>
     * @param {string} apellidos the las name of the profesor <apellidos>
     * @param {int} numAlumnos the number of alumnos in class
     * @param {object} grupos the object array containing grupos
     */
    addNewClase = (idProfesor, apellidos, numAlumnos, grupos) => {
        //var clase = this.createClase(idProfesor, apellidos, numAlumnos, grupos);
        var clase = this.Clase.createClase(idProfesor, apellidos, numAlumnos, grupos);
        this.addClase(clase);
    }

    //#endregion

    /**
     * Returns the final object to send to the service
     * @returns {object} the complete data object
     */
    static createReturnData = () => {
        var data = {
            profesores: this.profesores,
            clases: this.clases,
            aulas: this.aulas
        }
        console.log(data);
        return data
    }


}