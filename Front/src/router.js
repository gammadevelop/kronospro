import Vue from 'vue'
import Router from 'vue-router'
import Formulario from './views/Formulario.vue'
import TeacherForm from './views/TeacherForm.vue'
import ScheduleComponent from './components/ScheduleComponent'
import Disponibilidad from './views/Disponibility.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/profesores'
    },
    {
      path: '/index.html',
      redirect: '/',
    },
    {
      path: '*',
      redirect: '/',
    },
    {
      path: '/profesores',
      name: 'Teacher',
      component: TeacherForm
    },
    {
      path: '/aulas',
      name: 'Formulario',
      component: Formulario
    },
    {
      path: '/ScheduleComponent',
      name: 'Schedule',
      component: ScheduleComponent
    }
  ]
})
