import Vue from 'vue'
// import App from './App.vue'
import AppFormulario from './AppFormulario.vue'
import router from './router'

import './../node_modules/bulma/css/bulma.css';

Vue.config.productionTip = false

new Vue({
  router,
  //render: h => h(App),
  render: h => h(AppFormulario),
}).$mount('#app')

