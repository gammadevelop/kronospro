// Class that represents a generator that generates schedules based on a schema.
class Generator {
  /*
    Creates a generator.
    @param {data} - The data received from the submited form, to generate a schedule.
   */
  constructor(data) {
    this._data = data;
    this._scheduleSchema;
  }

  /*
    Setter for the property scheduleSchema.
    @param {newScheduleSchema} - The new value for the property scheduleSchema.
   */
  set scheduleSchema(newScheduleSchema) {
    this._scheduleSchema = newScheduleSchema;
  }

  /*
    Getter for the property scheduleSchema.
    @return {scheduleSchema} - The property scheduleSchema.
   */
  get scheduleSchema() {
    return this._scheduleSchema;
  }

  /*
    Getter for the property data.
    @return {data} - The property data.
  */
  get data() {
    return this._data;
  }

  /*
    Gets the classes asociated with a teacher from data.clases.
    @param {teacher} - The teacher to use for fetching it's associated classes.
    @return {classesOfTeacher} - The classes of the teacher.
   */
  getClassesOfTeacher(teacher) {
    const classesOfTeacher = [];

    for (let x = 0; x < this.data.clases.length; x += 1) {
      if (this.data.clases[x].profesor === teacher.id) {
        for (let y = 0; y < this.data.clases.length; y += 1) {
          if (this.data.clases[x][y] !== undefined) classesOfTeacher.push(this.data.clases[x][y]);
        }
      }
    }
    return classesOfTeacher;
  }

  /*
    Returns a random class selected from the teacher's classes.
    @param {teacher} - The teacher to use for fetching associated classes.
    @param {classesOfTeacher} - The classes of a teacher.
    @return {randomClass} - A class radomly selected from the teacher's classes.
   */
  pickRandomClass(teacher, classesOfTeacher) {
    const min = Math.ceil(0);
    const max = Math.floor(classesOfTeacher.length);
    const randomClass = classesOfTeacher[Math.floor(Math.random() * (max - min) + min)];

    return {
      grupo: randomClass.nombre_grupo,
      nombre: randomClass.nombre_asignatura,
      profesor: `${teacher.nombre} ${teacher.apellidos}`,
    };
  }

  /*
    Adds a class to the final schedule.
    @param {hour} - The hour where to insert the class.
    @param {day} - The day where to insert the class.
    @param {teacher} - The teacher associated with the class.
    @param {classesOfTeacher} - The classes associated with the teacher.
   */
  pushToFinalSchema(hour, day, teacher, classesOfTeacher) {
    this.scheduleSchema
      .schedule[hour][day].groups.push(this.pickRandomClass(teacher, classesOfTeacher));
  }

  /*
    Generates a random schedule for a single teacher.
    @param {teacher} - The teacher used to generate a schedule.
   */
  generateScheduleForSingleTeacher(teacher) {
    const classesOfTeacher = this.getClassesOfTeacher(teacher);
    const disponibilityOfTeacher = teacher.disponibilidad;

    /*
      x = Hour.
      y = Day.
    */
    for (let x = 0; x < disponibilityOfTeacher.length; x += 1) {
      for (let y = 0; y < disponibilityOfTeacher[x].length; y += 1) {
        if (disponibilityOfTeacher[x][y]) this.pushToFinalSchema(x, y, teacher, classesOfTeacher);
      }
    }
  }

  /*
    Generates a random schedule for a list of teachers.
    @return {scheduleSchema} - New generated random schedule for the list of teachers.
   */
  generateSchedules() {
    this.initialize(24);
    for (let x = 0; x < this.data.profesores.length; x += 1) {
      this.generateScheduleForSingleTeacher(this.data.profesores[x]);
    }
    return this.scheduleSchema;
  }

  /*
    Initializes the generation of the schedule's schema.
    @param {lengthOfSchema} - The length for generating the schedule's schema.
   */
  initialize(lengthOfSchema) {
    const newScheduleSchema = {
      schedule: {},
    };

    for (let x = 0; x < lengthOfSchema; x += 1) {
      newScheduleSchema.schedule[x] = [{
          name: 'lunes',
          groups: [],
        },
        {
          name: 'martes',
          groups: [],
        },
        {
          name: 'miercoles',
          groups: [],
        },
        {
          name: 'jueves',
          groups: [],
        },
        {
          name: 'viernes',
          groups: [],
        },
      ];
    }

    this.scheduleSchema = newScheduleSchema;
  }
}

export default Generator;