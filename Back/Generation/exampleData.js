const exampleData = {
  profesores: [{
      id: 'jon.snow',
      nombre: 'Jon',
      apellidos: 'Snow',
      disponibilidad: [
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
      ],
    },
    {
      id: 'daenerys.targaryen',
      nombre: 'Daenerys',
      apellidos: 'Targaryen',
      disponibilidad: [
        [1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
      ],
    },
  ],
  clases: [{
      profesor: 'sansa.stark',
      apellidos_profesor: 'Stark',
      total_alumnos: 50,
      0: {
        nombre_grupo: 'IIAA3201',
        nombre_asignatura: 'Inglés III',
        total_alumnos_grupo: 40,
      },
      1: {
        nombre_grupo: 'CMAMO1201',
        nombre_asignatura: 'Inglés I',
        total_alumnos_grupo: 10,
      },
    },
    {
      profesor: 'daenerys.targaryen',
      apellidos_profesor: 'Targaryen',
      total_alumnos: 50,
      0: {
        nombre_grupo: 'IIAA3201',
        nombre_asignatura: 'Inglés III',
        total_alumnos_grupo: 40,
      },
      1: {
        nombre_grupo: 'CMAMO1201',
        nombre_asignatura: 'Inglés I',
        total_alumnos_grupo: 10,
      },
    },
    {
      profesor: 'eddard.stark',
      apellidos_profesor: 'Stark',
      total_alumnos: 50,
      0: {
        nombre_grupo: 'IIAA3201',
        nombre_asignatura: 'Inglés III',
        total_alumnos_grupo: 40,
      },
      1: {
        nombre_grupo: 'CMAMO1201',
        nombre_asignatura: 'Inglés I',
        total_alumnos_grupo: 10,
      },
    },
    {
      profesor: 'jon.snow',
      apellidos_profesor: 'Snow',
      total_alumnos: 50,
      0: {
        nombre_grupo: 'IIAA3201',
        nombre_asignatura: 'Inglés III',
        total_alumnos_grupo: 40,
      },
      1: {
        nombre_grupo: 'CMAMO1201',
        nombre_asignatura: 'Inglés I',
        total_alumnos_grupo: 10,
      },
    }
  ],
};

export default exampleData;