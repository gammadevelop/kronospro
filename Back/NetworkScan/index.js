var cmd = require("node-cmd");
var IPs = {};

cmd.get("arp -a", function (err, data, stderr) {
    // console.log("IPs: ", data);
    let regex = /Interfaz: ([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3})\s|([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3})\s/gm;
    let m;
    let actualInterface;

    while ((m = regex.exec(data)) !== null) {

        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }

        // The result can be accessed through the `m`-variable.
        m.forEach((match, groupIndex) => {
            if (groupIndex == 1 && match != undefined) {
                actualInterface = match;
                IPs[actualInterface] = [];

                // console.log('Found match, group ' + groupIndex + ': ' + match);
            } else if (groupIndex == 2 && match != undefined) {

                IPs[actualInterface].push(match);

            }
        });
    }
    // console.log(IPs);
    for (inter in IPs) {
        console.log("Interface:  ", inter);
        for (let i = 0; i < IPs[inter].length; i++) {
            console.log('ping ', IPs[inter][i]);
        }
    }
});