/**
 * +-----------------------------------------+-----------+ 
 * |            Code flow                    |  Status   |
 * |-----------------------------------------+-----------+
 * |1-Forms initialize this script.          | (Mocked)  |
 * |                                         |           |
 * |2-Generate the random schedule.          | (Done)    |
 * |                                         |           |
 * |3-Trigger OnDataUpdate & OndataChange    | (Soon...) |
 * |in order to update all peers data        |           |
 * |and refresh all peers view.              |           |
 * +-----------------------------------------+-----------+ 
 */

const Generator = require('../ExtensionBack/Generation/generation');

// initialize();

function initialize() {

    //Instance of the randomizer class (Generator)
    const generator = new Generator(getInitialData());

    //Randomizing classes in the schedule
    var generatedSchedule = generator.generateSchedules();

    //TOFIX: Remove this line
    console.log(JSON.stringify(generatedSchedule));
}

/**
 * Get the data from the forms
 * of teachers and classrooms
 * 
 * @param <Void>
 * @return <JSON> initial data
 * 
 * TOFIX: This function should return
 * the forms's data (teachers and classrooms),
 * replace the exampleData with the real data.
 */
function getInitialData() {
    return exampleData = require('../ExtensionBack/Generation/exampleData');
}

/**
 * Triggers two events in order to
 * update all peers data and refresh
 * all peers frontend
 * 
 * @param <Void>
 * @return <Void>
 * 
 * TOFIX: Replace the mockData with the
 * real schedule data
 */
function firstPeersUpdate() {

    //Mock of the data
    var mockData = {
        "isMock": true,
        "isExample": true,
        "isRealData": false
    };

    //Creating the events
    var onDataUpdate = new CustomEvent("OnDataUpdate", {
        detail: mockData
    });
    var onDataChange = new CustomEvent("OnDataChange", {
        detail: mockData
    });

    //Dispatching the events
    document.dispatchEvent(onDataUpdate);
    document.dispatchEvent(onDataChange);
}