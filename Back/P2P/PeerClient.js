class PeerClient {
  constructor(id) {
    this.peer = new Peer(id, {
      key: "lwjd5qra8257b9"
    });
    this.peerList = [];
    this.conn = null;
    this._setPeerListeners();
  }

  /**
   * Listeners to be setted in the
   * Peer object
   *
   * @param <void>
   * @return <void>
   */
  _setPeerListeners() {
    //Function when the peer receives a new connection
    this.peer.on("connection", c => {
      console.log("New conenction", c);
      this.conn = c;
      this._setConnListeners();

      /**
       * TOFIX: This should wait the connection to be stablished first
       */
      this.sendData("New node");
    });

    //Function if the peer fails connecting
    this.peer.on("error", err => {
      console.error(err);
    });

    //Function when the connection is closed
    this.peer.on("close", () => {
      console.warn("Connection closed");
    });
  }

  /**
   * Listeners to be setted in the
   * Peer's connections
   *
   * @param <void>
   * @return <void>
   */
  _setConnListeners() {
    //Function called when the data tunnel is created
    this.conn.on("open", () => {
      console.info("Peer connection established with", this.conn.peer);
      // this.sendData("New node");
    });

    //Function called when the peer receives data from other peers
    this.conn.on("data", data => {
      console.warn("Received", data, data.peerList);
      this._updatePeerList(data.peerList);
    });
  }

  /**
   * Function that sends data to al the other peers
   *
   * @param <string> message
   * @return <void>
   */
  sendData(message) {
    let data = {
      peerList: this.peerList,
      who: this.peer.id,
      data: message
    };
    for (let peerConn in this.getConnections()) {
      console.log("sending to ", peerConn);
      this.getConnections()[peerConn][0].send(data);
    }
    console.log("data sended");
  }

  /**
   * Function that connects to other peer
   *
   * @param <string> target
   * @return <void>
   */
  connectTo(target) {
    console.log("Trying to connect with ", target);

    //Connects to other peer
    this.conn = this.peer.connect(target);

    //Updates his own peerList to add the new peer
    this.peerList = this.getConnectedPeers();

    //Add the listeners to the new peer's connection
    this._setConnListeners();
  }

  /**
   * @param <Void>
   * @return <Object> All connections of the peer
   */
  getConnections() {
    return this.peer.connections;
  }

  /**
   * @param <Void>
   * @return <Array> All connections of the peer
   *                         in an array of strings
   */
  getConnectedPeers() {
    return Object.keys(this.getConnections());
  }

  /**
   * This function compares his own connections list
   * to a received list from other peer in order to add
   * the ones that are not connected with and to connect
   *
   * @param <Array> Other peer's connections list
   * @return <Void>
   */
  _updatePeerList(listReceived) {
    console.log("LR", listReceived);

    //Adding new peers from outside
    for (let i = 0; i < listReceived.length; i++) {
      /**Verify if the local list has all the external peers,
       * if this is undefined, this peers has no connection
       * with the other peer so it will connect
       */
      let fPeer = this.peerList.find(pl => pl == listReceived[i]);

      //Checking if the peer is in the list and if the peer is no himself
      if (fPeer == undefined && listReceived[i] != this.peer.id) {
        console.log("New peer detected");

        //Adding the new peer to the list
        this.peerList.push(listReceived[i]);

        //Connecting to the new peer
        this.connectTo(listReceived[i]);
      }
    }
  }
}
